import React from "react";
import { ActivityIndicator, View } from "react-native";

function Loading() {
  return (
    <View style={{flex: 1, justifyContent: "center", backgroundColor: "#212121"}}>
      <ActivityIndicator size="large" color="#ffc400" />
    </View>
  );
}

export default Loading;