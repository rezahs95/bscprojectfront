export const url = `http://10.0.2.2:3000`;

export const departments = [
  { name: "کامپیوتر", color: "#c7b800aa" },
  { name: "عمران", color: "#880e4faa" },
  { name: "مکانیک", color: "#4a148caa" },
  { name: "برق", color: "#1a237eaa" },
  { name: "نفت", color: "#01579baa" },
  { name: "صنایع", color: "#006064aa" },
  { name: "دریا", color: "#1b5e20aa" },
  { name: "شیمی", color: "#827717aa" },
  { name: "متالورژی", color: "#f57f17aa" },
  { name: "پلیمر", color: "#b71c1caa" },
  { name: "پزشکی", color: "#5d4037aa" },
  { name: "ریاضی", color: "#424242aa" },
  { name: "فیزیک", color: "#34515eaa" },
  { name: "معدن", color: "#0093c4aa" },
  { name: "نساجی", color: "#ff4081aa" },
  { name: "هوافضا", color: "#7cb342aa" }
];