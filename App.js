import React, { createContext, useState, useEffect } from "react";
import Storage from "react-native-local-storage";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "./screens/loginScreen";
import HomeScreen from "./screens/homeScreen";
import UsersScreen from "./screens/users/usersScreen";
import ProgramScreen from "./screens/profile/programScreen";
import Loading from "./components/loading";

const Stack = createStackNavigator();

export const UserContext = createContext();
export const UserDispatcherContext = createContext();

function App() {

  const [firstUser, setFirstUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    Storage.get("firstUser").then((res) => {
      setFirstUser(res);
      setLoading(false);
    }).catch(() => {
      setLoading(false);
    });
    console.log(firstUser);
  }, []);

  if(loading)
    return <Loading />;

  return (
    <UserContext.Provider value={{ firstUser }}>
      <UserDispatcherContext.Provider value={{ setFirstUser }}>
        <NavigationContainer>
          <Stack.Navigator>
            {
              firstUser == null ?
                (
                  <>
                    <Stack.Screen
                      name="Login"
                      options={{
                        title: "ارتباط با استاد",
                        headerTitleAlign: "center",
                        headerTintColor: "#ffc400",
                        headerStyle: {
                          backgroundColor: "#212121"
                        }
                      }}
                      component={LoginScreen}
                    />
                    <Stack.Screen
                      name="Guest"
                      options={{
                        title: "پروفایل اساتید",
                        headerTintColor: "#ffc400",
                        headerStyle: {
                          backgroundColor: "#212121"
                        }
                      }}
                    >
                      {(props) => <UsersScreen {...props} guest />}
                    </Stack.Screen>
                    <Stack.Screen
                      name="Program"
                      options={{
                        title: "برنامه هفتگی",
                        headerTintColor: "#ffc400",
                        headerStyle: {
                          backgroundColor: "#212121"
                        }
                      }}
                      component={ProgramScreen}
                    />
                  </>
                )
                :
                (
                  <Stack.Screen
                    name="Home"
                    options={{ headerShown: false }}
                    component={HomeScreen}
                  />
                )
            }
          </Stack.Navigator>
        </NavigationContainer>
      </UserDispatcherContext.Provider>
    </UserContext.Provider>
  );
}

export default App;