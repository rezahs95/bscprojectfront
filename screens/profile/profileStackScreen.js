import React, { useContext } from "react";
import { TouchableOpacity, View, Image, Alert } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Storage from "react-native-local-storage";
import ProfileScreen from "./profileScreen";
import ProgramScreen from "./programScreen";
import EditScreen from "./editScreen";
import EditProgramScreen from "./editProgramScreen";
import { UserDispatcherContext } from "../../App";
import logout from "../../assets/logout.png";
import edit from "../../assets/edit.png";

const ProfileStack = createStackNavigator();

function ProfileStackScreen({ navigation }) {

  const { setFirstUser } = useContext(UserDispatcherContext);

  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profile"
        options={{
          headerRight: () => (
            <View style={{ flexDirection: "row-reverse" }}>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    "",
                    "آیا می‌خواهید خارج شوید؟",
                    [
                      { text: "خیر", onPress: () => console.log("Cancel Pressed"), style: "cancel" },
                      {
                        text: "بله", onPress: () => {
                          Storage.remove("firstUser");
                          setFirstUser(null);
                        }
                      }
                    ],
                    { cancelable: false }
                  );
                }
                }
              >
                <Image
                  source={logout}
                  style={{ width: 30, height: 30, margin: 15 }}
                  tintColor="#ffc400"
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.navigate("Edit")}
                title="Edit"
              >
                <Image
                  source={edit}
                  style={{ width: 30, height: 30, margin: 15, marginRight: 10 }}
                  tintColor="#ffc400"
                />
              </TouchableOpacity>
            </View>
          ),
          title: "پروفایل",
          headerTitleAlign: "center",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={ProfileScreen}
      />
      <ProfileStack.Screen
        name="Edit"
        options={{
          title: "ویرایش پروفایل",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={EditScreen}
      />
      < ProfileStack.Screen
        name="Program"
        options={{
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate("EditProgram")}
            >
              <Image
                source={edit}
                style={{ width: 30, height: 30, margin: 15, marginRight: 10 }}
                tintColor="#ffc400"
              />
            </TouchableOpacity>
          ),
          title: "برنامه هفتگی",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={ProgramScreen}
      />
      < ProfileStack.Screen
        name="EditProgram"
        options={{
          title: "ویرایش برنامه هفتگی",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={EditProgramScreen}
      />
    </ProfileStack.Navigator>
  );
}

export default ProfileStackScreen;