import React, { useState, useEffect, useCallback, useContext } from "react";
import { ScrollView, View, Text } from "react-native";
import axios from "axios";
import { useFocusEffect } from "@react-navigation/native";
import { url } from "../../setting";
import { UserContext } from "../../App";
import Loading from "../../components/loading";

const days = {
  0: "شنبه",
  1: "یک‌شنبه",
  2: "دوشنبه",
  3: "سه‌شنبه",
  4: "چهارشنبه",
  5: "پنج‌شنبه"
};

function ProgramScreen({ route }) {

  const [program, setProgram] = useState(null);
  const { firstUser } = useContext(UserContext);

  const getUser = useCallback((id) => {
    if(firstUser) {
      axios.get(`${url}/user/profile/${id}`, {
        headers: {
          "x-authorization": firstUser.token
        }
      })
        .then((resp) => {
          setProgram(resp.data.data.user.program);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      axios.get(`${url}/user/profile/${id}`)
        .then((resp) => {
          setProgram(resp.data.data.user.program);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [url, setProgram]);

  useEffect(() => {
    getUser(route.params);
  }, [getUser]);

  useFocusEffect(useCallback(() => {
    getUser(route.params);
  }, [getUser]));

  if(!program)
    return <Loading />;

  return (
    <ScrollView contentContainerStyle={{ backgroundColor: "#212121", flexGrow: 1 }}>
      <View style={{ flexDirection: "column", alignItems: "center", padding: 20 }}>
        {Object.keys(program).map((day) => (
          <View
            style={{
              width: "100%",
              marginBottom: 20,
              alignItems: "flex-end",
              backgroundColor: "#ffc400",
              borderRadius: 10
            }} key={day}
          >
            <View style={{ margin: 10 }}>
              <Text style={{ fontSize: 24, fontWeight: "bold", color: "#212121" }}>
                {days[day] + ":"}
              </Text>
            </View>
            {program[day] && program[day].map((time) => (
              <View
                style={{
                  margin: 10,
                  flexDirection: "row-reverse",
                  justifyContent: "space-between",
                  width: "100%"
                }} key={time.from}
              >
                <Text style={{ color: "#212121", fontWeight: "bold", marginRight: 10, fontSize: 16 }}>
                  {time.from + "  تا  " + time.to}
                </Text>
                <Text style={{ color: "#212121", marginLeft: 20, fontWeight: "bold", fontSize: 16 }}>{time.prog}</Text>
              </View>
            ))}
          </View>
        ))}
      </View>
    </ScrollView>
  );
}

export default ProgramScreen;