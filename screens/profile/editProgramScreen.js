import React, { useState, useEffect, useCallback, useContext } from "react";
import { ScrollView, View, Text, Alert, TouchableOpacity, StyleSheet, Image, TextInput } from "react-native";
import { Picker } from "@react-native-community/picker";
import axios from "axios";
import { UserContext } from "../../App";
import { url } from "../../setting";
import Loading from "../../components/loading";
import cross from "../../assets/cross.png";
import plus from "../../assets/plus.png";

const days = {
  0: "شنبه",
  1: "یک‌شنبه",
  2: "دوشنبه",
  3: "سه‌شنبه",
  4: "چهارشنبه",
  5: "پنج‌شنبه"
};

function EditProgramScreen({ navigation }) {

  const [program, setProgram] = useState(null);
  const [newItem, setNewItem] = useState({
    day: null,
    fromm: null,
    fromh: null,
    tom: null,
    toh: null,
    prog: null
  });
  const { firstUser } = useContext(UserContext);

  const getUser = useCallback((id) => {
    axios.get(`${url}/user/profile/${id}`, {
      headers: {
        "x-authorization": firstUser.token
      }
    })
      .then((resp) => {
        setProgram(resp.data.data.user.program);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [url, setProgram]);

  useEffect(() => {
    getUser(firstUser.user.id);
  }, [getUser]);

  if(!program)
    return <Loading />;

  return (
    <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{ backgroundColor: "#212121", flexGrow: 1 }}>
      <View style={{ flexDirection: "column", alignItems: "center", padding: 20 }}>
        <View
          style={{
            width: "100%",
            marginBottom: 20,
            alignItems: "flex-end",
            backgroundColor: "#ffc400",
            borderRadius: 10
          }}
        >
          <Text style={{ fontSize: 24, fontWeight: "bold", color: "#212121", margin: 10 }}>
            اضافه کردن برنامه:
          </Text>
          <View
            style={{
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
              margin: 10
            }}
          >
            <View style={{ flexDirection: "row-reverse", alignItems: "center" }}>
              <Text style={{ fontWeight: "bold" }}>روز: </Text>
              <View style={{ borderRadius: 10, overflow: "hidden", margin: 10 }}>
                <Picker
                  style={[styles.usersPicker, { width: 180 }]}
                  selectedValue={newItem.day}
                  onValueChange={(itemValue) => {
                    setNewItem({ ...newItem, day: itemValue });
                  }}
                >
                  <Picker.Item label="..." value="" key="-1" />
                  <Picker.Item label="شنبه" value="0" key="0" />
                  <Picker.Item label="یک‌شنبه" value="1" key="1" />
                  <Picker.Item label="دوشنبه" value="2" key="2" />
                  <Picker.Item label="سه‌شنبه" value="3" key="3" />
                  <Picker.Item label="چهارشنبه" value="4" key="4" />
                  <Picker.Item label="پنج‌شنبه" value="5" key="5" />
                </Picker>
              </View>
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "center", alignItems: "center", margin: 10 }}>
              <Text
                style={{
                  color: "#212121",
                  fontWeight: "bold",
                  fontSize: 15
                }}
              >از ساعت </Text>
              <TextInput
                keyboardType="number-pad"
                maxLenght={2}
                style={{ borderWidth: 2, backgroundColor: "white", height: 40, width: 40, padding: 10 }}
                onChangeText={(text) => {
                  setNewItem({ ...newItem, fromm: text });
                }}
              />
              <Text
                style={{
                  color: "#212121",
                  fontWeight: "bold",
                  fontSize: 15
                }}
              > : </Text>
              <TextInput
                keyboardType="number-pad"
                maxLenght={2}
                style={{ borderWidth: 2, backgroundColor: "white", height: 40, width: 40, padding: 10 }}
                onChangeText={(text) => {
                  setNewItem({ ...newItem, fromh: text });
                }}
              />
              <Text
                style={{
                  color: "#212121",
                  fontWeight: "bold",
                  fontSize: 15
                }}
              > تا </Text>
              <TextInput
                keyboardType="number-pad"
                maxLenght={2}
                style={{ borderWidth: 2, backgroundColor: "white", height: 40, width: 40, padding: 10 }}
                onChangeText={(text) => {
                  setNewItem({ ...newItem, tom: text });
                }}
              />
              <Text
                style={{
                  color: "#212121",
                  fontWeight: "bold",
                  fontSize: 15
                }}
              > : </Text>
              <TextInput
                keyboardType="number-pad"
                maxLenght={2}
                style={{ borderWidth: 2, backgroundColor: "white", height: 40, width: 40, padding: 10 }}
                onChangeText={(text) => {
                  setNewItem({ ...newItem, toh: text });
                }}
              />
            </View>
            <View style={{ flexDirection: "row-reverse", alignItems: "center", margin: 10 }}>
              <Text
                style={{
                  color: "#212121",
                  fontWeight: "bold",
                  fontSize: 15
                }}
              >برنامه: </Text>
              <TextInput
                style={{ borderWidth: 2, backgroundColor: "white", height: 45, width: 220, padding: 5 }}
                onChangeText={(text) => {
                  setNewItem({ ...newItem, prog: text });
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  if(newItem.day && newItem.fromh && newItem.fromm && newItem.toh && newItem.tom && newItem.prog) {
                    const da = newItem.day;
                    const pro = JSON.parse(JSON.stringify(program));
                    pro[da].push({
                      from: newItem.fromh + ":" + newItem.fromm,
                      to: newItem.toh + ":" + newItem.tom,
                      prog: newItem.prog
                    });
                    if(program[da].filter(a => a.from === newItem.fromh + ":" + newItem.fromm).length !== 0) {
                      Alert.alert(
                        "",
                        "زمان تکراری است!",
                        [
                          { text: "باشه" }
                        ],
                        { cancelable: false }
                      );
                    } else {
                      setProgram({ ...pro });
                    }
                  } else
                    Alert.alert(
                      "",
                      "همه فیلدها را پر کنید!",
                      [
                        { text: "باشه" }
                      ],
                      { cancelable: false }
                    );
                }}
                title="Edit"
              >
                <Image
                  source={plus}
                  style={{ width: 40, height: 40, marginLeft: 20, marginRight: 10 }}
                  tintColor="darkgreen"
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {Object.keys(program).map((day) => (
          <View
            style={{
              width: "100%",
              marginBottom: 20,
              alignItems: "flex-end",
              backgroundColor: "#ffc400",
              borderRadius: 10
            }} key={day}
          >
            <Text style={{ fontSize: 24, fontWeight: "bold", color: "#212121", margin: 10 }}>
              {days[day] + ":"}
            </Text>
            {program[day] && program[day].map((time, index) => (
              <View
                style={{
                  margin: 10,
                  flexDirection: "row-reverse",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "100%"
                }} key={time.from}
              >
                <Text style={{ color: "#212121", fontWeight: "bold", fontSize: 15, marginRight: 10 }}>
                  {time.from + "  تا  " + time.to}
                </Text>
                <View style={{ flexDirection: "row-reverse", alignItems: "center" }}>
                  <Text
                    style={{
                      color: "#212121",
                      fontWeight: "bold",
                      fontSize: 15
                    }}
                  >{time.prog}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      const pro2 = JSON.parse(JSON.stringify(program));
                      pro2[day].splice(index, 1);
                      setProgram({ ...pro2 });
                    }}
                  >
                    <Image
                      source={cross}
                      style={{ width: 20, height: 20, marginLeft: 20, marginRight: 10 }}
                      tintColor="#c62828"
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ))}
          </View>
        ))}
        <TouchableOpacity
          style={styles.loginTouchableOpacity}
          onPress={() => {
            axios.put(`${url}/user/edit`, { program }, {
              headers: {
                "x-authorization": firstUser.token
              }
            })
              .then(() => {
                navigation.navigate("Program");
              })
              .catch((err) => {
                console.log(err);
                Alert.alert(
                  "",
                  "مشکلی پیش آمده است!",
                  [
                    { text: "باشه" }
                  ],
                  { cancelable: false }
                );
              });
          }}
        >
          <Text style={styles.loginButtonText}>ویرایش</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  loginTouchableOpacity: {
    width: "100%",
    height: 50,
    margin: 10,
    marginBottom: 20,
    backgroundColor: "#ffc400",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  loginButtonText: {
    color: "#212121",
    fontSize: 20
  },
  usersPicker: {
    width: 270,
    height: 40,
    backgroundColor: "#212121aa",
    color: "white"
  }
});

export default EditProgramScreen;