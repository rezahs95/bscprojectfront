import React, { useCallback, useState, useEffect, useContext } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View, ScrollView } from "react-native";
import { url } from "../../setting";
import { useFocusEffect } from "@react-navigation/native";
import avatar from "../../assets/avatar.png";
import axios from "axios";
import { UserContext } from "../../App";
import Loading from "../../components/loading";

function ProfileScreen({ navigation }) {

  const { firstUser } = useContext(UserContext);
  const [user, setUser] = useState(firstUser && firstUser.user);

  const {
    id,
    imageUrl,
    firstName,
    lastName,
    department,
    email,
    status,
    levelOfStudy,
    username,
    role
  } = user;

  const getUser = useCallback((id) => {
    axios.get(`${url}/user/profile/${id}`, {
      headers: {
        "x-authorization": firstUser.token
      }
    })
      .then((resp) => {
        setUser(resp.data.data.user);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [url, setUser]);

  useEffect(() => {
      getUser(id);
    }
    , []);

  useFocusEffect(useCallback(() => {
    getUser(id);
  }, []));

  if(!user)
    return <Loading />;

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="always">
      <View style={styles.profileContainer}>
        <Image
          style={styles.profileImage}
          source={imageUrl ? { uri: `${url}/${imageUrl}` } : avatar}
          resizeMode="contain"
        />
        <Text style={styles.profileTextTitle}>{firstName + " " + lastName}</Text>
        {role === "STUDENT" &&
        <>
          <Text style={styles.profileText}>{"شماره دانشجویی: " + username}</Text>
          <Text style={styles.profileText}>{"مقطع: " + levelOfStudy}</Text>
          <Text style={styles.profileText}>{"رشته: " + department}</Text>
        </>
        }
        {role === "TEACHER" &&
        <Text style={styles.profileText}>{"دانشکده: " + department}</Text>
        }
        <Text style={styles.profileText}>{"ایمیل: " + email}</Text>
        {role === "TEACHER" &&
        <Text style={styles.profileText}>{"وضعیت: " + status}</Text>
        }
        {role === "TEACHER" &&
        <View style={styles.profileContainer3}>
          <TouchableOpacity
            style={styles.profileTouchableOpacity}
            onPress={() => {
              navigation.navigate("Program", id);
            }}
          >
            <Text style={styles.profileButtonText}>برنامه</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.profileTouchableOpacity}
            onPress={() => {
            }}
          >
            <Text style={styles.profileButtonText}>پیش‌بینی حضور</Text>
          </TouchableOpacity>
        </View>
        }
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    backgroundColor: "#ffc400",
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "flex-start"
  },
  profileContainer2: {
    flexDirection: "row-reverse",
    marginBottom: 15,
    marginTop: 5
  },
  profileContainer3: {
    flexDirection: "row-reverse",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: 10
  },
  profileContainer4: {
    flexDirection: "row-reverse"
  },
  profileImage: {
    width: "100%",
    height: "50%",
    marginBottom: 16,
    borderWidth: 2,
    borderColor: "#212121",
    backgroundColor: "#ffc400"
  },
  profileTextTitle: {
    color: "black",
    textAlign: "right",
    marginRight: 16,
    fontSize: 32,
    fontWeight: "bold",
    marginBottom: 16
  },
  profileText: {
    color: "black",
    textAlign: "right",
    fontSize: 20,
    marginRight: 16
  },
  profileTouchableOpacity: {
    width: "40%",
    height: 50,
    backgroundColor: "#212121",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginRight: 16,
    marginBottom: 10
  },
  profileButtonText: {
    color: "#ffc400",
    fontSize: 20
  }
});
export default ProfileScreen;