import React, { useState, useContext, useCallback, useEffect } from "react";
import { Text, TextInput, View, StyleSheet, Image, TouchableOpacity, ScrollView, Alert } from "react-native";
import ImagePicker from "react-native-image-picker";
import axios from "axios";
import { url } from "../../setting";
import { UserContext } from "../../App";
import avatar from "../../assets/avatar.png";

function EditScreen({ navigation }) {
  const { firstUser } = useContext(UserContext);
  const [edit, setEdit] = useState({});
  const [user, setUser] = useState(firstUser && firstUser.user);

  const {
    id,
    imageUrl,
    role
  } = user;

  const options = {
    title: "انتخاب عکس پروفایل",
    cancelButtonTitle: "بستن",
    takePhotoButtonTitle: "عکس گرفتن با دوربین",
    chooseFromLibraryButtonTitle: "انتخاب عکس از گالری"
  };

  const getUser = useCallback((id) => {
    axios.get(`${url}/user/profile/${id}`, {
      headers: {
        "x-authorization": firstUser.token
      }
    })
      .then((resp) => {
        setUser(resp.data.data.user);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [url, setUser]);

  useEffect(() => {
      getUser(id);
    }
    , []);

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="always">
      <View style={{ backgroundColor: "#212121", flex: 1, flexDirection: "column", alignItems: "center" }}>
        <TouchableOpacity
          onPress={() => {
            ImagePicker.showImagePicker(options, (response) => {
              if(response.didCancel) {
                console.log("User cancelled image picker");
              } else if(response.error) {
                Alert.alert(
                  "",
                  "مشکلی پیش آمده است!",
                  [
                    { text: "باشه" }
                  ],
                  { cancelable: false }
                );
              } else {
                const formData = new FormData();
                formData.append("avatar", response.data);
                axios.put(`${url}/user/avatar`, formData, {
                  headers: {
                    "x-authorization": firstUser.token
                  }
                })
                  .then(() => {
                    navigation.navigate("Profile");
                  })
                  .catch(() => {
                    Alert.alert(
                      "",
                      "مشکلی پیش آمده است!",
                      [
                        { text: "باشه" }
                      ],
                      { cancelable: false }
                    );
                  });
              }
            });
          }}
        >
          <Image
            style={styles.chatImage}
            source={imageUrl ? { uri: `${url}/${imageUrl}` } : avatar}
          />
        </TouchableOpacity>
        <Text style={{ fontWeight: "bold", color: "#ffc400", marginBottom: 5 }}>ایمیل:</Text>
        <TextInput
          style={styles.messageTextInput}
          onChangeText={(text) => {
            setEdit({ ...edit, email: text });
          }}
        />
        <Text style={{ fontWeight: "bold", color: "#ffc400", marginBottom: 5 }}>رمز عبور:</Text>
        <TextInput
          style={styles.messageTextInput}
          onChangeText={(text) => {
            setEdit({ ...edit, password: text });
          }}
        />
        {role === "TEACHER" &&
        <View style={styles.messageContainer}>
          <Text style={{ fontWeight: "bold", color: "#ffc400", marginBottom: 5 }}>وضعیت:</Text>
          <TextInput
            style={[styles.messageTextInput, { textAlign: "right", marginBottom: 40 }]}
            onChangeText={(text) => {
              setEdit({ ...edit, status: text });
            }}
          />
        </View>
        }
        <TouchableOpacity
          style={styles.loginTouchableOpacity}
          onPress={() => {
            axios.put(`${url}/user/edit`, edit, {
              headers: {
                "x-authorization": firstUser.token
              }
            })
              .then(() => {
                navigation.navigate("Profile");
              })
              .catch(() => {
                Alert.alert(
                  "",
                  "مشکلی پیش آمده است!",
                  [
                    { text: "باشه" }
                  ],
                  { cancelable: false }
                );
              });
          }}
        >
          <Text style={styles.loginButtonText}>ویرایش</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  messageContainer: {
    flexDirection: "column",
    alignItems: "center"
  },
  messageTextInput: {
    backgroundColor: "#ffc400aa",
    width: 300,
    height: 45,
    paddingRight: 10,
    paddingLeft: 10,
    color: "#212121",
    borderRadius: 10,
    marginBottom: 20
  },
  chatImage: {
    width: 200,
    height: 200,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "#ffc400",
    marginTop: 20,
    marginBottom: 25
  },
  loginTouchableOpacity: {
    width: 300,
    height: 50,
    marginBottom: 10,
    backgroundColor: "#ffc400",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  loginButtonText: {
    color: "#212121",
    fontSize: 20
  }
});


export default EditScreen;