import React, { useEffect, useState } from "react";
import { Image } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Storage from "react-native-local-storage";
import ProfileStackScreen from "./profile/profileStackScreen";
import ChatStackScreen from "./chat/chatStackScreen";
import UsersStackScreen from "./users/usersStackScreen";
import profile from "../assets/profile.png";
import chat from "../assets/chat.png";
import search from "../assets/search.png";
import Loading from "../components/loading";

const Tab = createBottomTabNavigator();

function HomeScreen() {

  const [firstUser, setFirstUser] = useState(null);

  useEffect(() => {
    Storage.get("firstUser").then((res) => {
      setFirstUser(res);
    });
  }, []);

  if(!firstUser)
    return <Loading />;

  return (
      <Tab.Navigator
        tabBarOptions={{
          keyboardHidesTabBar: false,
          activeBackgroundColor: "#484848",
          inactiveBackgroundColor: "#212121",
          showLabel: false
        }}
      >
        <Tab.Screen
          name="ProfileStack"
          options={{
            tabBarIcon: () => (
              <Image source={profile} style={{ width: 35, height: 35 }} tintColor="#ffc400" />
            )
          }}
          component={ProfileStackScreen}
        />
        <Tab.Screen
          name="ChatStack"
          options={{
            tabBarIcon: () => (
              <Image source={chat} style={{ width: 35, height: 35 }} tintColor="#ffc400" />
            )
          }}
          component={ChatStackScreen}
        />
        <Tab.Screen
          name="UsersStack"
          options={{
            tabBarIcon: () => (
              <Image source={search} style={{ width: 35, height: 35 }} tintColor="#ffc400" />
            )
          }}
          component={UsersStackScreen}
        />
      </Tab.Navigator>

  );
}

export default HomeScreen;