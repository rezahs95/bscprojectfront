import React, { useContext, useState } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
  Alert,
  Image,
  ScrollView
} from "react-native";
import axios from "axios";
import Storage from "react-native-local-storage";
import { UserDispatcherContext } from "../App";
import { url } from "../setting";
import amirkabir from "../assets/amirkabir.png";

function LoginScreen({ navigation }) {

  const { setFirstUser } = useContext(UserDispatcherContext);
  const [login, setLogin] = useState({});

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="always">
      <View style={styles.loginContainer}>
        <Image source={amirkabir} style={styles.amirkabirImage} />
        <TextInput
          style={styles.loginTextInput}
          placeholder="نام کاربری"
          placeholderTextColor='#212121'
          onChangeText={(text) => {
            setLogin({ ...login, username: text });
          }}
        />
        <TextInput
          style={styles.loginTextInput}
          secureTextEntry
          placeholder="رمز عبور"
          placeholderTextColor='#212121'
          onChangeText={(text) => {
            setLogin({ ...login, password: text });
          }}
        />
        <TouchableOpacity
          style={styles.loginTouchableOpacity}
          onPress={() => {
            axios.post(`${url}/user/login`, login)
              .then((resp) => {
                Storage.save("firstUser", resp.data.data).then(() => {
                  Storage.get("firstUser").then((res) => {
                    setFirstUser(res);
                  });
                }).catch((err) => {
                  console.log(err);
                });
              })
              .catch(() => {
                Alert.alert(
                  "",
                  "نام کاربری یا رمز عبور اشتباه است!",
                  [
                    { text: "باشه" }
                  ],
                  { cancelable: false }
                );
              });
          }}
        >
          <Text style={styles.loginButtonText}>ورود</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.loginTouchableOpacity, { backgroundColor: "#484848" }]}
          onPress={() => navigation.navigate("Guest")}
        >
          <Text style={styles.loginButtonText}>مهمان</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    backgroundColor: "#ffc400",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  amirkabirImage: {
    width: 200,
    height: 200,
    margin: 10
  },
  loginTextInput: {
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 1,
    textAlign: "right",
    width: 300,
    height: 50,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5
  },
  loginTouchableOpacity: {
    width: 300,
    height: 50,
    marginBottom: 10,
    backgroundColor: "#212121",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  loginButtonText: {
    color: "#ffc400",
    fontSize: 20
  }
});

export default LoginScreen;