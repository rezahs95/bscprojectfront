import React, { useCallback, useEffect, useState, useContext } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView
} from "react-native";
import { Picker } from "@react-native-community/picker";
import axios from "axios";
import { UserContext } from "../../App";
import { url, departments } from "../../setting";
import Loading from "../../components/loading";
import avatar from "../../assets/avatar.png";
import searchImg from "../../assets/search.png";

function UsersScreen({ navigation, guest }) {

  const { firstUser } = useContext(UserContext);
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState(false);
  const [picked, setPicked] = useState(null);
  const [dep, setDep] = useState("ALL");
  const [rol, setRol] = useState("ALL");
  const [search, setSearch] = useState(null);

  const getUsers = useCallback((rol, dep, search) => {
    if(firstUser && firstUser.user.role === "TEACHER") {
      if(rol === "ALL")
        axios.get(`${url}/user/`, {
          headers: { "x-authorization": firstUser.token }
        })
          .then((resp) => {
            setUsers(resp.data.data.users.filter((value) =>
              (value.id !== firstUser.user.id &&
                (dep !== "ALL" ? value.department === dep : true) &&
                (search ?
                  value.firstName.includes(search) ||
                  value.lastName.includes(search) ||
                  (firstUser.user.role === "TEACHER" && value.username.includes(search)) : true))));
          })
          .catch((err) => {
            console.log(err);
          });
      else if(rol === "TEACHER")
        axios.get(`${url}/user/`, {
          headers: {
            "x-authorization": firstUser.token
          },
          params: {
            role: "TEACHER"
          }
        })
          .then((resp) => {
            setUsers(resp.data.data.users.filter((value) =>
              (value.id !== firstUser.user.id &&
                (dep !== "ALL" ? value.department === dep : true) &&
                (search ?
                  value.firstName.includes(search) ||
                  value.lastName.includes(search) ||
                  (firstUser.user.role === "TEACHER" && value.username.includes(search)) : true))));
          })
          .catch((err) => {
            console.log(err);
          });
      else if(rol === "STUDENT")
        axios.get(`${url}/user/`, {
          headers: {
            "x-authorization": firstUser.token
          },
          params: {
            role: "STUDENT"
          }
        })
          .then((resp) => {
            setUsers(resp.data.data.users.filter((value) =>
              (value.id !== firstUser.user.id &&
                (dep !== "ALL" ? value.department === dep : true) &&
                (search ?
                  value.firstName.includes(search) ||
                  value.lastName.includes(search) ||
                  (firstUser.user.role === "TEACHER" && value.username.includes(search)) : true))));
          })
          .catch((err) => {
            console.log(err);
          });
    } else if(firstUser && firstUser.user.role === "STUDENT") {
      axios.get(`${url}/user/`, {
        headers: {
          "x-authorization": firstUser.token
        },
        params: {
          role: "TEACHER"
        }
      })
        .then((resp) => {
          setUsers(resp.data.data.users.filter((value) =>
            (value.id !== firstUser.user.id &&
              (dep !== "ALL" ? value.department === dep : true) &&
              (search ?
                value.firstName.includes(search) ||
                value.lastName.includes(search) ||
                (firstUser.user.role === "TEACHER" && value.username.includes(search)) : true))));
        })
        .catch((err) => {
          console.log(err);
        });
    } else if(!firstUser && guest) {
      axios.get(`${url}/user/`, {
        params: {
          role: "TEACHER"
        }
      })
        .then((resp) => {
          setUsers(resp.data.data.users.filter((value) =>
            ((dep !== "ALL" ? value.department === dep : true) &&
              (search ?
                value.firstName.includes(search) ||
                value.lastName.includes(search) ||
                (value.username.includes(search)) : true))));
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [url, setUsers]);

  const getUser = useCallback((value) => {
    if(guest)
      axios.get(`${url}/user/profile/${value}`)
        .then((resp) => {
          setUser(resp.data.data.user);
        })
        .catch((err) => {
          console.log(err);
        });
    else if(firstUser)
      axios.get(`${url}/user/profile/${value}`, {
        headers: {
          "x-authorization": firstUser.token
        }
      })
        .then((resp) => {
          setUser(resp.data.data.user);
        })
        .catch((err) => {
          console.log(err);
        });
  }, [url, setUser]);

  useEffect(() => {
    getUsers(rol, dep, search);
  }, [getUsers]);

  if(guest) {
    if(!users)
      return <Loading />;
  } else {
    if(!users || !firstUser)
      return <Loading />;
  }

  const {
    id,
    firstName,
    lastName,
    imageUrl,
    department,
    email,
    status,
    levelOfStudy,
    username,
    role
  } = user;

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="always">
      <View style={styles.usersContainer}>
        <View style={{ flexDirection: "row-reverse", alignItems: "center", marginTop: 10, marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>جست‌وجوی کاربر: </Text>
          <View style={styles.messageContainer}>
            <TouchableOpacity
              style={styles.messageTouchableOpacity}
              onPress={() => {
                getUsers(rol, dep, search);
              }}
            >
              <Image
                source={searchImg}
                style={styles.messageImage3}
                tintColor="#ffc400"
              />
            </TouchableOpacity>
            <TextInput
              style={styles.messageTextInput}
              defaultValue={search}
              onChangeText={(text) => {
                setSearch(text);
              }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row-reverse", alignItems: "center" }}>
          {firstUser && firstUser.user.role === "TEACHER" &&
          <>
            <Text style={{ fontWeight: "bold" }}>نشان دادن: </Text>
            <View style={{ borderRadius: 10, overflow: "hidden" }}>
              <Picker
                selectedValue={rol}
                style={[styles.usersPicker, { width: 75 }]}
                onValueChange={(itemValue) => {
                  setRol(itemValue);
                  getUsers(itemValue, dep, search);
                }}
              >
                <Picker.Item label="همه" value="ALL" key="all" />
                <Picker.Item label="استادها" value="TEACHER" key="teacher" />
                <Picker.Item label="دانشجوها" value="STUDENT" key="student" />
              </Picker>
            </View>
          </>
          }
          <Text style={{ fontWeight: "bold", marginRight: firstUser && firstUser.user.role === "TEACHER" ? 30 : 0 }}>از
            دانشکده: </Text>
          <View style={{ borderRadius: 10, overflow: "hidden", marginLeft: 0 }}>
            <Picker
              selectedValue={dep}
              style={[styles.usersPicker, { width: firstUser && firstUser.user.role === "TEACHER" ? 75 : 240 }]}
              onValueChange={(itemValue) => {
                setDep(itemValue);
                getUsers(rol, itemValue, search);
              }}
            >
              <Picker.Item label="همه" value="ALL" key="all" />
              {departments.map((depar) => (
                <Picker.Item label={depar.name} value={depar.name} key={depar.color} />
              ))}
            </Picker>
          </View>
        </View>
        <View style={{ flexDirection: "row-reverse", alignItems: "center" }}>
          <Text style={{ fontWeight: "bold" }}>کاربر: </Text>
          <View style={{ borderRadius: 10, overflow: "hidden", marginTop: 10, marginBottom: 10 }}>
            <Picker
              selectedValue={picked}
              style={styles.usersPicker}
              onValueChange={(itemValue) => {
                if(itemValue !== "ALL") {
                  setPicked(itemValue);
                  getUser(itemValue);
                }
              }}
            >
              <Picker.Item label="..." value="ALL" key="all" />
              {users.map((user) => (
                <Picker.Item
                  label={user.firstName + " " + user.lastName + (user.role === "STUDENT" ? " - " + user.username : "")}
                  value={user.id}
                  key={user.id}
                />
              ))}
            </Picker>
          </View>
        </View>
        {user && (
          <View style={styles.usersContainer5}>
            <Image
              style={styles.usersImage}
              source={imageUrl ? { uri: `${url}/${imageUrl}` } : avatar}

            />
            <Text style={styles.usersTextTitle}>{firstName + " " + lastName}</Text>
            {role === "STUDENT" &&
            <>
              <View style={styles.usersContainer4}>
                <Text style={styles.usersText}>{"شماره دانشجویی: "}</Text>
                <Text style={styles.usersText}>{username}</Text>
              </View>
              <Text style={styles.usersText}>{"مقطع: " + levelOfStudy}</Text>
              <Text style={styles.usersText}>{"رشته: " + department}</Text>
            </>
            }
            {role === "TEACHER" &&
            <Text style={styles.usersText}>{"دانشکده: " + department}</Text>
            }
            <Text style={styles.usersText}>{"ایمیل: " + email}</Text>
            {role === "TEACHER" &&
            <View style={styles.usersContainer2}>
              <Text style={styles.usersText}>{"وضعیت: "}</Text>
              <Text style={styles.usersText}>{status}</Text>
            </View>
            }
            {role === "TEACHER" &&
            <View style={styles.usersContainer3}>
              <TouchableOpacity
                style={styles.usersTouchableOpacity}
                onPress={() => {
                  navigation.navigate("Program", id);
                }}
              >
                <Text style={styles.usersButtonText}>برنامه هفتگی</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.usersTouchableOpacity}
                onPress={() => {
                }}
              >
                <Text style={styles.usersButtonText}>پیش‌بینی حضور</Text>
              </TouchableOpacity>
            </View>}
            <>
              {
                !guest &&
                <TouchableOpacity
                  style={[styles.usersTouchableOpacity, { width: 310, marginTop: role === "STUDENT" ? 10 : 0 }]}
                  onPress={() => {
                    navigation.navigate("Message", {
                      id: id,
                      username: username,
                      department: department,
                      firstName: firstName,
                      lastName: lastName,
                      imageUrl: imageUrl,
                      role: role
                    });
                  }}
                >
                  <Text style={styles.usersButtonText}>ارسال پیام</Text>
                </TouchableOpacity>
              }
            </>
          </View>
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  usersContainer: {
    flex: 1,
    backgroundColor: "#ffc400",
    flexDirection: "column",
    alignItems: "center"
  },
  usersContainer5: {
    backgroundColor: "#ffc400",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 10,
    padding: 20
  },
  usersContainer2: {
    flexDirection: "row-reverse",
    marginTop: 5,
    marginBottom: 10
  },
  usersContainer3: {
    flexDirection: "row-reverse",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginBottom: 10
  },
  usersContainer4: {
    flexDirection: "row-reverse"
  },
  usersPicker: {
    width: 270,
    height: 40,
    backgroundColor: "#212121aa",
    color: "white"
  },
  usersImage: {
    width: 180,
    height: 180,
    marginBottom: 10,
    backgroundColor: "#ffc400",
    borderRadius: 30
  },
  usersTextTitle: {
    color: "black",
    textAlign: "right",
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 5
  },
  usersText: {
    color: "black",
    textAlign: "right",
    fontSize: 18
  },
  usersTouchableOpacity: {
    width: 150,
    height: 45,
    backgroundColor: "#212121",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginRight: 5,
    marginLeft: 5
  },
  usersButtonText: {
    color: "#ffc400",
    fontSize: 20
  },
  loginTextInput: {
    backgroundColor: "white",
    borderColor: "#212121",
    borderWidth: 1,
    width: 240,
    height: 45,
    marginRight: 10,
    padding: 10,
    borderRadius: 5
  },
  messageTextInput: {
    backgroundColor: "#212121",
    width: 160,
    height: 45,
    paddingRight: 10,
    paddingLeft: 10,
    color: "white",
    textAlign: "right",
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10
  },
  messageTouchableOpacity: {
    width: 45,
    height: 45,
    backgroundColor: "#212121",
    alignItems: "center",
    justifyContent: "center",
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
  },
  messageImage3: {
    width: 25,
    height: 25
  },
  messageContainer: {
    flexDirection: "row",
    alignItems: "center"
  }
});

export default UsersScreen;