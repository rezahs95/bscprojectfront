import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import UsersScreen from "./usersScreen";
import ProgramScreen from "../profile/programScreen";
import MessageScreen from "../chat/messageScreen";

const UsersStack = createStackNavigator();

function UsersStackScreen({}) {
  return (
    <UsersStack.Navigator initialRouteName="Users">
      <UsersStack.Screen
        name="Users"
        options={{
          title: "جست‌وجو",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={UsersScreen}
      />
      <UsersStack.Screen
        name="Program"
        options={{
          title: "برنامه هفتگی",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={ProgramScreen}
      />
      <UsersStack.Screen
        name="Message"
        options={{
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121",
            height: 90
          }
        }}
        component={MessageScreen}
      />
    </UsersStack.Navigator>
  );
}

export default UsersStackScreen;