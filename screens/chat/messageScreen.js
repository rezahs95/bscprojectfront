import React, { useCallback, useState, useEffect, useContext } from "react";
import { StyleSheet, Text, TouchableOpacity, Image, TextInput, View, FlatList, Alert } from "react-native";
import axios from "axios";
import moment from "moment";
import { UserContext } from "../../App";
import { url } from "../../setting";
import Loading from "../../components/loading";
import send from "../../assets/send.png";
import seen from "../../assets/seen.png";
import avatar from "../../assets/avatar.png";
import refresh from "../../assets/refresh.png";

function MessageScreen({ navigation, route }) {

  const {
    id,
    firstName,
    lastName,
    imageUrl
  } = route.params;

  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState("");
  const { firstUser } = useContext(UserContext);

  const getMessages = useCallback((id) => {
    axios.get(`${url}/message/${id}`, {
      headers: { "x-authorization": firstUser.token }
    })
      .then((resp) => {
        setMessages(resp.data.data.messages.reverse());
      })
      .catch(() => {
        Alert.alert(
          "",
          "مشکلی پیش آمده است!",
          [
            { text: "باشه" }
          ],
          { cancelable: false }
        );
      });
  }, [url, setMessages]);

  const sendMessage = useCallback((message) => {
    if(message)
      axios.post(`${url}/message/send`, {
        "receiverId": id,
        "message": message
      }, {
        headers: { "x-authorization": firstUser.token }
      })
        .then(() => {
          getMessages(id);
        })
        .catch(() => {
          Alert.alert(
            "",
            "مشکلی پیش آمده است!",
            [
              { text: "باشه" }
            ],
            { cancelable: false }
          );
        });
  }, [url, setMessages]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={styles.messageContainer3}>
          <TouchableOpacity
            onPress={() => {
              getMessages(id);
            }}
          >
            <Image
              source={refresh}
              style={{ width: 30, height: 30, margin: 15, marginRight: 10 }}
              tintColor="#ffc400"
            />
          </TouchableOpacity>
        </View>
      ),
      headerTitle: () => (
        <View style={styles.messageContainer3}>
          <Image
            style={styles.messageImage}
            source={imageUrl ? { uri: `${url}/${imageUrl}` } : avatar}
          />
          <Text style={styles.messageHeaderText}>{firstName + " " + lastName}</Text>
        </View>
      )
    });
    getMessages(id);
  }, [getMessages]);

  if(!messages)
    return <Loading />;

  return (
    <>
      <FlatList
        style={styles.messagesStyle}
        inverted
        data={messages}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => {
          if(item.senderId === firstUser.user.id)
            return <View style={styles.messageContainer}>
              <Text />
              <View style={styles.messageContainer2}>
                <Text style={styles.messageMe}>{item.body}</Text>
                <View style={styles.messageContainer3}>
                  <Text style={styles.messageText}>
                    {moment(item.createdAt).format("MMMM Do YY, HH:mm")}
                  </Text>
                  <Image
                    source={seen}
                    style={styles.messageImage2}
                    tintColor={item.seen ? "#ffc400" : "#8e8e8eaa"}
                  />
                </View>
              </View>
            </View>;
          else return <View style={styles.messageContainer3}>
            <View style={styles.messageContainer4}>
              <Text style={styles.messageYou}>{item.body}</Text>
              <Text style={styles.messageText2}>
                {moment(item.createdAt).format("MMMM Do YY, HH:mm")}
              </Text>
            </View>
            <Text />
          </View>;
        }}
      />
      <View style={styles.messageContainer}>
        <TouchableOpacity
          style={styles.messageTouchableOpacity}
          onPress={() => {
            sendMessage(message);
            setMessage("");
          }}
        >
          <Image
            source={send}
            style={styles.messageImage3}
            tintColor="#ffc400"
          />
        </TouchableOpacity>
        <TextInput
          style={styles.messageTextInput}
          defaultValue={message}
          onChangeText={(text) => {
            setMessage(text);
          }}
        />
      </View>
    </>
  )
    ;
}

const styles = StyleSheet.create({
  messageHeaderText: {
    color: "#ffc400",
    fontSize: 20
  },
  messagesStyle: {
    backgroundColor: "white"
  },
  messageContainer: {
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  messageContainer2: {
    alignItems: "flex-end"
  },
  messageContainer3: {
    flexDirection: "row",
    alignItems: "center"
  },
  messageContainer4: {
    alignItems: "flex-start"
  },
  messageText: {
    fontSize: 10,
    color: "#21212166"
  },
  messageText2: {
    marginLeft: 10,
    fontSize: 10,
    color: "#21212166"
  },
  messageImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#ffc400",
    marginRight: 15
  },
  messageImage2: {
    width: 18,
    height: 18,
    marginRight: 10,
    marginLeft: 5
  },
  messageImage3: {
    width: 25,
    height: 25
  },
  messageMe: {
    backgroundColor: "#c79400",
    borderRadius: 15,
    margin: 10,
    marginBottom: 0,
    padding: 12,
    fontSize: 16,
    textAlign: "right"
  },
  messageYou: {
    backgroundColor: "#212121",
    color: "#ffc400",
    borderRadius: 15,
    margin: 10,
    marginBottom: 0,
    padding: 12,
    fontSize: 16,
    textAlign: "left"
  },
  messageTextInput: {
    backgroundColor: "#212121",
    width: "87%",
    height: 55,
    paddingRight: 10,
    paddingLeft: 10,
    color: "white"
  },
  messageTouchableOpacity: {
    width: "13%",
    height: 55,
    backgroundColor: "#212121",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default MessageScreen;