import React, { useCallback, useState, useEffect, useContext } from "react";
import { StyleSheet, ScrollView, Text, TouchableOpacity, Image, View, Alert } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import axios from "axios";
import { url, departments } from "../../setting";
import Loading from "../../components/loading";
import avatar from "../../assets/avatar.png";
import newMessage from "../../assets/newMessage.png";
import { UserContext } from "../../App";
import refresh from "../../assets/refresh.png";

function ChatScreen({ navigation }) {

  const [chats, setChats] = useState([]);
  const { firstUser } = useContext(UserContext);

  const getChats = useCallback(() => {
    axios.get(`${url}/message/`, {
      headers: { "x-authorization": firstUser.token }
    })
      .then((resp) => {
        setChats(resp.data.data.chats);
      })
      .catch(() => {
        Alert.alert(
          "",
          "مشکلی پیش آمده است!",
          [
            { text: "باشه" }
          ],
          { cancelable: false }
        );
      });
  }, [url, setChats]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={styles.messageContainer3}>
          <TouchableOpacity
            onPress={() => {
              getChats();
            }}
          >
            <Image
              source={refresh}
              style={{ width: 30, height: 30, margin: 15, marginRight: 10 }}
              tintColor="#ffc400"
            />
          </TouchableOpacity>
        </View>
      )
    });
    getChats();
  }, [getChats]);

  useFocusEffect(useCallback(() => {
    getChats();
  }, []));

  if(!chats)
    return <Loading />;

  return (
    <ScrollView>
      {chats.map((chat) => (
        <TouchableOpacity
          key={chat.id}
          style={styles.chatStyle}
          onPress={() => navigation.navigate("Message", chat)}
        >
          <View style={styles.chatContainer}>
            <Image
              style={styles.chatImage}
              source={chat.imageUrl ? { uri: `${url}/${chat.imageUrl}` } : avatar}
            />
            {chat.role === "TEACHER" ?
              <View style={{ flexDirection: "row-reverse" }}>
                <Text style={styles.chatText2}>{chat.firstName + " " + chat.lastName}</Text>
                <Text
                  style={[styles.chatTag, {
                    backgroundColor: departments.find(dep =>
                      (chat.department === dep.name)) ?
                      departments.find(dep =>
                        (chat.department === dep.name)).color :
                      "white"
                  }]}
                >
                  {chat.department}
                </Text>
              </View>
              :
              <View style={{ alignItems: "flex-end", justifyContent: "center" }}>
                <View style={{ flexDirection: "row-reverse" }}>
                  <Text style={styles.chatText}>{chat.firstName + " " + chat.lastName}</Text>
                  <Text
                    style={[styles.chatTag, {
                      backgroundColor: departments.find(dep =>
                        (chat.department === dep.name)) ?
                        departments.find(dep =>
                          (chat.department === dep.name)).color :
                        "white"
                    }]}
                  >
                    {chat.department}
                  </Text>
                </View>
                <Text style={styles.chatText3}>{chat.username}</Text>
              </View>
            }
          </View>
          {!chat.seen ?
            <Image source={newMessage} style={styles.chatTag2} tintColor="#c62828" /> :
            <></>
          }
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  chatStyle: {
    width: "100%",
    height: 70,
    backgroundColor: "white",
    flexDirection: "row-reverse",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    margin: 0,
    borderBottomWidth: 0.2
  },
  chatContainer: {
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  chatImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginLeft: 16,
    borderWidth: 1,
    borderColor: "#ffc400"
  },
  chatTag: {
    borderRadius: 5,
    padding: 2,
    paddingLeft: 5,
    paddingRight: 5,
    color: "#212121",
    fontSize: 15,
    marginRight: 10
  },
  chatTag2: {
    width: 25,
    height: 25,
    marginLeft: 10
  },
  chatText: {
    fontSize: 18,
    color: "#212121",
    fontWeight: "bold"
  },
  chatText2: {
    fontSize: 20,
    color: "#c79400",
    fontWeight: "bold"
  },
  chatText3: {
    fontSize: 16
  },
  messageContainer3: {
    flexDirection: "row",
    alignItems: "center"
  }
});

export default ChatScreen;