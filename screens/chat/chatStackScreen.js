import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ChatScreen from "./chatScreen";
import MessageScreen from "./messageScreen";

const ChatStack = createStackNavigator();

function ChatStackScreen() {

  return (
    <ChatStack.Navigator initialRouteName="Chat">
      <ChatStack.Screen
        name="Chat"
        options={{
          title: "گفت‌وگوها",
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121"
          }
        }}
        component={ChatScreen}
      />
      <ChatStack.Screen
        name="Message"
        options={{
          headerTintColor: "#ffc400",
          headerStyle: {
            backgroundColor: "#212121",
            height: 90
          }
        }}
        component={MessageScreen}
      />
    </ChatStack.Navigator>
  );
}

export default ChatStackScreen;